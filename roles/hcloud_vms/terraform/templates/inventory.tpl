all:
  hosts:
%{ for master in masters ~}
    ${master.name}:
      ansible_host: ${master.ipv4_address}
      ansible_python_interpreter: "/usr/bin/python3"
%{ endfor ~}
%{ for node in nodes ~}
    ${node.name}:
      ansible_host: ${node.ipv4_address}
      ansible_python_interpreter: "/usr/bin/python3"
%{ endfor ~}
  children:
    master:
      hosts:
%{ for master in masters ~}
        ${master.name}:
%{ endfor ~}
    node:
      hosts:
%{ for node in nodes ~}
        ${node.name}:
%{ endfor ~}
    k3s_cluster:
      children:
        master:
        node:
